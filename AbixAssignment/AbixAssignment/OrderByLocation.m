//
//  OrderByLocation.m
//  AbixAssignment
//
//  Created by Spire Jankulovski on 12/24/16.
//  Copyright © 2016 Spire Jankulovski. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "OrderByLocation.h"

@implementation OrderByLocation

- (NSArray *) returnArraySortedByLocations:(NSMutableArray *)locations {
    
    imageLocations = locations;
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0 &&
        [CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedWhenInUse
        //[CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways
        ) {
        // Will open an confirm dialog to get user's approval
        [locationManager requestWhenInUseAuthorization];
        //[_locationManager requestAlwaysAuthorization];
    } else {
        [locationManager startUpdatingLocation]; //Will update location immediately
    }
    
    
    return imageLocations;

}    
#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager*)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined: {
            NSLog(@"User still thinking..");
        } break;
        case kCLAuthorizationStatusDenied: {
            NSLog(@"User Denied");
        } break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        case kCLAuthorizationStatusAuthorizedAlways: {
            [locationManager startUpdatingLocation]; //Will update location immediately
        } break;
        default:
            break;
    }
}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    [imageLocations sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        // Edit 3: verbose comparator.
        float dist1 =[(CLLocation*)obj1 distanceFromLocation:currentLocation];
        float dist2 = [(CLLocation*)obj2 distanceFromLocation:currentLocation];
        if (dist1 == dist2) {
            return NSOrderedSame;
        }
        else if (dist1 < dist2) {
            return NSOrderedAscending;
        }
        else {
            return NSOrderedDescending;
        }
    }];


}
@end
