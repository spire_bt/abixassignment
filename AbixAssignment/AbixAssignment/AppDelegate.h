//
//  AppDelegate.h
//  AbixAssignment
//
//  Created by Spire Jankulovski on 12/24/16.
//  Copyright © 2016 Spire Jankulovski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

