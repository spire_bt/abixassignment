//
//  ViewController.h
//  AbixAssignment
//
//  Created by Spire Jankulovski on 12/24/16.
//  Copyright © 2016 Spire Jankulovski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate>

- (IBAction)arrangeImagesOrder:(id)sender;
@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) IBOutlet UISegmentedControl *arrangeImages;
@property (strong, nonatomic) IBOutlet UISegmentedControl * segmentedControl;
@end

