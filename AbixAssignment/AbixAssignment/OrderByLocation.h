//
//  OrderByLocation.h
//  AbixAssignment
//
//  Created by Spire Jankulovski on 12/24/16.
//  Copyright © 2016 Spire Jankulovski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface OrderByLocation : NSObject <CLLocationManagerDelegate>{
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    NSMutableArray *imageLocations;
}
- (NSArray *) returnArraySortedByLocations:(NSMutableArray *)locations;

@end
