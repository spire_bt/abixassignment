//
//  ViewController.m
//  AbixAssignment
//
//  Created by Spire Jankulovski on 12/24/16.
//  Copyright � 2016 Spire Jankulovski. All rights reserved.
//

#import "ViewController.h"
#import "ImageCollectionViewCell.h"
#import "OrderByLocation.h"

@interface ViewController (){
    NSArray *recipeImages;
    NSMutableArray *jsonDataArray;
    BOOL isFullScreen;
    UIImageView *imageView;
    CGRect prevFrame;
    UITapGestureRecognizer *tapper;
    UIView *blackView;
    OrderByLocation *orderingObject;

}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
   
    [self createDataArray];
    isFullScreen = false;
    imageView = [[UIImageView alloc] init];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [imageView setClipsToBounds:YES];
    imageView.userInteractionEnabled = YES;
    
    tapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imgToFullScreen:)];
    tapper.numberOfTapsRequired = 1;
    
    [imageView addGestureRecognizer:tapper];
    
    blackView = [[UIView alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
    [blackView setBackgroundColor:[UIColor blackColor]];
    [blackView setAlpha:0.5f];
    [blackView setHidden:true];
    [self.view insertSubview:blackView belowSubview:imageView];
    orderingObject = [[OrderByLocation alloc] init];
    
    [self.view addSubview:imageView];
}

-(void)viewWillAppear:(BOOL)animated{
    [self.collectionView registerNib:[UINib nibWithNibName:@"ImageCollectionViewCell" bundle:[NSBundle mainBundle]]
        forCellWithReuseIdentifier:@"Cell"];
}
#pragma Mark:Image component
-(void)imgToFullScreen:(id)sender {
    if (!isFullScreen) {
        [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
            //save previous frame
            prevFrame = imageView.frame;
            [imageView setFrame:[[UIScreen mainScreen] bounds]];
        }completion:^(BOOL finished){
            isFullScreen = true;
            [blackView setHidden:false];
        }];
        return;
    }
    else{
        [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
            [imageView setFrame:prevFrame];
        }completion:^(BOOL finished){
            isFullScreen = false;
            [blackView setHidden:true];
        }];
        return;
    }
}
#pragma Mark: Data manipulation
-(void)createDataArray{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"resources" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    jsonDataArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
}
- (IBAction)arrangeImagesOrder:(id)sender {
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        [self createDataArray];
        [self.collectionView reloadData];
    }else if (self.segmentedControl.selectedSegmentIndex == 1) {
        jsonDataArray = [self createMutableArray:[self reverseArray]];
        [self.collectionView reloadData];
    }else if (self.segmentedControl.selectedSegmentIndex == 2) {
        NSArray *tempArray = [orderingObject returnArraySortedByLocations:jsonDataArray];
        jsonDataArray = [self createMutableArray:tempArray];
        [self.collectionView reloadData];
    }
}
-(NSArray *)reverseArray {
    NSArray* reversedArray = [[jsonDataArray reverseObjectEnumerator] allObjects];
    return reversedArray;
}
- (NSMutableArray *)createMutableArray:(NSArray *)array
{
    return [NSMutableArray arrayWithArray:array];
}

#pragma Mark: Collection View
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return jsonDataArray.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    
    ImageCollectionViewCell *cell = (ImageCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];

    NSDictionary *element = [jsonDataArray objectAtIndex:indexPath.row];
    NSURL *imageUrl = [NSURL URLWithString:[element valueForKey:@"url"]];
    // set default user image while image is being downloaded
    cell.imageLabel.text = [element valueForKey:@"title"];
    // download the image asynchronously
    [self downloadImageWithURL:imageUrl completionBlock:^(BOOL succeeded, UIImage *image) {
        if (succeeded) {
            // change the image in the cell
            cell.chImageView.image = image;
        }
    }];
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSURL *imageUrl = [NSURL URLWithString:[[jsonDataArray objectAtIndex:indexPath.row] valueForKey:@"url"]];

    [self downloadImageWithURL:imageUrl completionBlock:^(BOOL succeeded, UIImage *image) {
        if (succeeded) {
            // change the image in the cell
            imageView.image = image;
            [self imgToFullScreen:nil];
            isFullScreen = true;
        }
    }];
}
#pragma Mark: Download
- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
