//
//  ImageCollectionViewCell.h
//  AbixAssignment
//
//  Created by Spire Jankulovski on 12/24/16.
//  Copyright © 2016 Spire Jankulovski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIImage *dImage;
@property (nonatomic, strong) IBOutlet UIImageView *chImageView;
@property (nonatomic, strong) IBOutlet UILabel *imageLabel;

@end
